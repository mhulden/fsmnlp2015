# -*- coding: utf-8 -*- 

from foma import *

def printparses(word):
    p = Grammar.parse(word)
    for parse in p:
        print parse

FST.define(u'{papi}|{wiʈæ}|{ŋuku}|{wanka}|{kaɾikaɾi}|{jukaɾpa}|{putuka}|{jilijili}|{kiʈikiʈi}|{muŋkumuŋku}', "Stems")

Lex = MTFSM(u'Stems ["[Acc. Nonfuture]":{in} | "[Acc. Future]":{uɻ} | "[Uninflected]":0 ]')

FST.define(u'[a | æ | i | u]', 'Vow')
FST.define(u'[p | t | ʈ | t̪ | tʲ |k | m | n | ɳ | ŋ | ŋ | n̪ | nʲ| ɾ | l | w | ɻ | j ]', 'Cons')
FST.define(u'[t | ʈ | n | ɳ | ɾ | l | ɻ ]', 'Apical')
FST.define(u'[m | n | ɳ | ŋ | ŋ | n̪ | nʲ]', 'Nasal')

FST.define(u'[Vow|Cons|"k-Epenthesis"|"w-Epenthesis"|"Vowel Deletion"|"Final Lowering"|"Apocope"|"Cluster Reduction"|"Non-Apical Deletion"|"Sonorantization"|"#"]', 'A')

FST.definef(('Mark', ('Rule', 'Label')), '[Rule .o. [~$["#"] 0:"#" 0:Label | \\"#"* "#" ?:Label]]')

kEpenthesis = MTFSM(u'A* .o. Mark([ [..] -> k || Nasal _ u ɻ ], "k-Epenthesis")')
wEpenthesis = MTFSM(u'A* .o. Mark([ [..] -> w || i _ u ] , "w-Epenthesis")')
VowelDeletion = MTFSM(u'A* .o. Mark([ Vow -> 0 || Vow _ ], "Vowel Deletion")')
FinalLowering = MTFSM(u'A* .o. Mark([ i -> æ, u -> a || _ [.#.|"#"] ], "Final Lowering")')
Apocope = MTFSM(u'A* .o. Mark([ Vow -> 0 || Vow Cons* Vow Cons* _ [.#.| "#"] ], "Apocope")')
ClusterRed = MTFSM(u'A* .o. Mark([ Cons -> 0 || Cons _ [.#.|"#"] ], "Cluster Reduction")')
NonApicalDel = MTFSM(u'A* .o. Mark([ [Cons - Apical] -> 0 || _ [.#.|"#"] ] , "Non-Apical Deletion")')
Sonorantization = MTFSM(u'A* .o. Mark([ ʈ -> ɻ || _ [.#.|"#"] ], "Sonorantization")')
Cleanup = MTFSM(u'A* .o. ?* "#":0 ?*:0')

Grammar = Lex + kEpenthesis + wEpenthesis + VowelDeletion + FinalLowering + Apocope + ClusterRed + NonApicalDel + Sonorantization + Cleanup

## Parse ##

printparses(u'muŋkumu')
printparses(u'putu')
