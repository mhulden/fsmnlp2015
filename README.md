
### README ###

Example code & grammar, slides for the paper:

```
#!BibTeX

@InProceedings{hulden:2015:FSMNLP,
  author    = {Hulden, Mans},
  title     = {Grammar design with multi-tape automata and composition},
  booktitle = {Proceedings of the 12th International Conference on Finite State Methods and Natural Language Processing},
  month     = {June},
  year      = {2015},
  address   = {Duesseldorf},
  publisher = {Association for Computational Linguistics}
}
```

Requires [foma](https://foma.googlecode.com) library installed.

### Contents ###

* Lardil grammar (lardil.py)
* Foma bindings for Python (foma.py, grammar relies on this)
* Slides
* Paper
